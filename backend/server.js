const express = require('express');
const routes = require('./routes');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('../dist'));
app.use('/api', routes);
app.use((req, res, next) => {
    next(new Error(404));
});
app.use((err, req, res, next) => {
    const statusCode = err.message;
    const response = {
        code: statusCode,
        stack: err.stack,
    };
  
    res.status(statusCode).send(response);
});

app.listen(8080, () => {
    console.log('Listening to port 8080');
});
