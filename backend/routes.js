const express = require('express');

const router = express.Router();

router.post('/contactForm', (req, res) => {
    res.status(201).send({
        msg: 'form sent',
        body: req.body
    });
});

module.exports = router;
